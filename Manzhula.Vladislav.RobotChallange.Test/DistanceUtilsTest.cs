﻿using System;
using Manzhula.Vladislav.RobotChallange.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace Manzhula.Vladislav.RobotChallange.Test
{
    [TestClass]
    public class DistanceUtilsTest
    {
        [TestMethod]
        public void TestGetDistanceMethod()
        {
            Position posFrom = new Position(10, 10);
            Position posTo = new Position(10, 15);

            Assert.AreEqual(25, DistanceUtils.getDistance(posFrom, posTo));
        }

        [TestMethod]
        public void TestGetToStationFastestWay()
        {
            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Energy = 100,
                Position = new Position(1, 1),
                OwnerName = "Manzhula Vladislav"
            };

            Position nearestStationPos = new Position(1, 10);
            Position middleStationPos = new Position(10, 10);
            Position fatherestStationPos = new Position(10, 15);

            Assert.AreEqual(nearestStationPos, DistanceUtils.getToStationFastestWay(nearestStationPos, robot));

            Assert.AreNotEqual(middleStationPos, DistanceUtils.getToStationFastestWay(middleStationPos, robot));
            Assert.AreEqual(new Position(5, 5), DistanceUtils.getToStationFastestWay(middleStationPos, robot));

            Assert.AreNotEqual(fatherestStationPos, DistanceUtils.getToStationFastestWay(fatherestStationPos, robot));
            Assert.AreEqual(new Position(4, 4), DistanceUtils.getToStationFastestWay(fatherestStationPos, robot));

        }
    }
}
