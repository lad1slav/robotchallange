﻿using System;
using System.Collections.Generic;
using Manzhula.Vladislav.RobotChallange.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace Manzhula.Vladislav.RobotChallange.Test
{
    [TestClass]
    public class FindUtilsTest
    {
        [TestMethod]
        public void TestGetNearestStation()
        {
            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Energy = 100,
                Position = new Position(1, 1),
                OwnerName = "Manzhula Vladislav"
            };

            Map map = new Map();

            EnergyStation nearestStation = new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(10, 10),
                RecoveryRate = 50
            };

            EnergyStation fatherestStation = new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(10, 15),
                RecoveryRate = 50
            };

            map.Stations.Add(nearestStation);
            map.Stations.Add(fatherestStation);

            Assert.AreEqual(nearestStation, FindUtils.getNearestStation(robot, map.Stations));
            Assert.AreNotEqual(fatherestStation, FindUtils.getNearestStation(robot, map.Stations));
        }

        [TestMethod]
        public void TestGetСellsInRadius()
        {
            Position pos = new Position(10, 10);

            Assert.AreEqual(9, FindUtils.getCellsInRadius(pos, 1).Count);
            Assert.AreEqual(25, FindUtils.getCellsInRadius(pos, 2).Count);
        }

        [TestMethod]
        public void TestGetNearestFreeStation()
        {
            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Energy = 100,
                Position = new Position(1, 1),
                OwnerName = "Manzhula Vladislav"
            };

            Map map = new Map();

            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();

            EnergyStation nearestOccupiedStation = new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(10, 10),
                RecoveryRate = 50
            };

            Robot.Common.Robot otherRobot = new Robot.Common.Robot()
            {
                Energy = 100,
                Position = nearestOccupiedStation.Position,
                OwnerName = "Manzhula Vladislav"
            };

            EnergyStation nearestFreeStation = new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(10, 14),
                RecoveryRate = 50
            };

            EnergyStation fatherestFreeStation = new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(10, 15),
                RecoveryRate = 50
            };

            map.Stations.Add(nearestOccupiedStation);
            map.Stations.Add(fatherestFreeStation);
            map.Stations.Add(nearestFreeStation);

            robots.Add(robot);
            robots.Add(otherRobot);

            Assert.AreNotEqual(fatherestFreeStation, FindUtils.getNearestFreeStation(robot, robots, map.Stations));
            Assert.AreNotEqual(nearestOccupiedStation, FindUtils.getNearestFreeStation(robot, robots, map.Stations));
            Assert.AreEqual(nearestFreeStation, FindUtils.getNearestFreeStation(robot, robots, map.Stations));
        }

        [TestMethod]
        public void TestIsСellFreeInRadius()
        {
            Position pos = new Position(10, 10);

            Position otherPos = new Position(10, 20);

            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Energy = 100,
                Position = new Position(1, 1),
                OwnerName = "Manzhula Vladislav"
            };

            Robot.Common.Robot otherRobot = new Robot.Common.Robot()
            {
                Energy = 100,
                Position = pos,
                OwnerName = "Manzhula Vladislav"
            };

            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();

            robots.Add(robot);

            robots.Add(otherRobot);

            Assert.IsFalse(FindUtils.isCellFreeInRadius(pos, robot, robots, 1));

            otherRobot.Position = otherPos;

            Assert.IsTrue(FindUtils.isCellFreeInRadius(pos, robot, robots, 1));
        }

        [TestMethod]
        public void TestGetRobotCountInRadius()
        {
            Position pos = new Position(10, 10);

            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Energy = 100,
                Position = pos,
                OwnerName = "Manzhula Vladislav"
            };

            Robot.Common.Robot otherRobot = new Robot.Common.Robot()
            {
                Energy = 100,
                Position = new Position(pos.X, pos.Y + 1),
                OwnerName = "Manzhula Vladislav"
            };

            Robot.Common.Robot oneMoreRobot = new Robot.Common.Robot()
            {
                Energy = 100,
                Position = new Position(pos.X - 1, pos.Y + 1),
                OwnerName = "Manzhula Vladislav"
            };

            Robot.Common.Robot robotInOtherArea = new Robot.Common.Robot()
            {
                Energy = 100,
                Position = new Position(4, 4),
                OwnerName = "Manzhula Vladislav"
            };

            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();

            robots.Add(robot);

            robots.Add(otherRobot);

            robots.Add(oneMoreRobot);

            robots.Add(robotInOtherArea);

            Assert.IsFalse(FindUtils.isCellFreeInRadius(pos, robot, robots, 1));
            Assert.AreEqual(3, FindUtils.getRobotCountInRadius(pos, robots, 1));
        }

        [TestMethod]
        public void TestGetFreeCellInRadius()
        {
            Position pos = new Position(10, 10);

            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Energy = 100,
                Position = new Position(1, 1),
                OwnerName = "Manzhula Vladislav"
            };

            Robot.Common.Robot otherRobot = new Robot.Common.Robot()
            {
                Energy = 100,
                Position = new Position(pos.X-1, pos.Y - 1),
                OwnerName = "Manzhula Vladislav"
            };

            Robot.Common.Robot oneMoreRobot = new Robot.Common.Robot()
            {
                Energy = 100,
                Position = new Position(pos.X - 1, pos.Y + 1),
                OwnerName = "Manzhula Vladislav"
            };

            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();

            robots.Add(robot);

            robots.Add(otherRobot);

            robots.Add(oneMoreRobot);

            Assert.AreNotEqual(otherRobot.Position, FindUtils.getFreeCellInRadius(pos, 1, robot, robots));
            Assert.AreNotEqual(oneMoreRobot.Position, FindUtils.getFreeCellInRadius(pos, 1, robot, robots));

            Assert.AreEqual(new Position(9, 10), FindUtils.getFreeCellInRadius(pos, 1, robot, robots));
        }
    }
}
