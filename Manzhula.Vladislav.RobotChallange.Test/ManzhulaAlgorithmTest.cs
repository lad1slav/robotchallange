﻿using System;
using System.Collections.Generic;
using Manzhula.Vladislav.RobotChallange.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace Manzhula.Vladislav.RobotChallange.Test
{
    [TestClass]
    public class ManzhulaAlgorithmTest
    {
        [TestMethod]
        public void TestDoStepGetToNearestStationOnStart()
        {
            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Energy = 100,
                Position = new Position(1, 1),
                OwnerName = "Manzhula Vladislav"
            };

            Map map = new Map();
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();

            EnergyStation nearestStation = new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(10, 10),
                RecoveryRate = 50
            };

            EnergyStation fatherestStation = new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(10, 15),
                RecoveryRate = 50
            };

            map.Stations.Add(nearestStation);
            map.Stations.Add(fatherestStation);

            robots.Add(robot);

            Assert.AreEqual(new MoveCommand() { NewPosition = new Position(5, 5) }.NewPosition,
                            ((MoveCommand)new ManzhulaAlgorithm().DoStep(robots, robots.IndexOf(robot), map)).NewPosition);

            nearestStation.Position = new Position(4, 4);

            Assert.AreEqual(new MoveCommand() { NewPosition = nearestStation.Position }.NewPosition,
                            ((MoveCommand)new ManzhulaAlgorithm().DoStep(robots, robots.IndexOf(robot), map)).NewPosition);

            Robot.Common.Robot otherRobot = new Robot.Common.Robot()
            {
                Energy = 100,
                Position = nearestStation.Position,
                OwnerName = "Manzhula Vladislav"
            };

            robots.Add(otherRobot);

            Assert.AreEqual(new MoveCommand() { NewPosition = FindUtils.getFreeCellInRadius(nearestStation.Position, 1, robot, robots)}.NewPosition,
                            ((MoveCommand)new ManzhulaAlgorithm().DoStep(robots, robots.IndexOf(robot), map)).NewPosition);

        }

        [TestMethod]
        public void TestDoStepNewRobotsCreations()
        {
            EnergyStation station = new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(10, 10),
                RecoveryRate = 50
            };

            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Energy = 322,
                Position = station.Position,
                OwnerName = "Manzhula Vladislav"
            };

            Map map = new Map();
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();

            EnergyStation fatherestStation = new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(10, 15),
                RecoveryRate = 50
            };

            map.Stations.Add(station);
            map.Stations.Add(fatherestStation);

            robots.Add(robot);

            ManzhulaAlgorithm algorithm = new ManzhulaAlgorithm();
            algorithm.RoundCount = 1;

            Assert.AreNotEqual(new CreateNewRobotCommand() { NewRobotEnergy = 500 }.NewRobotEnergy,
                            ((CreateNewRobotCommand)algorithm.DoStep(robots, robots.IndexOf(robot), map)).NewRobotEnergy);
            Assert.AreEqual(new CreateNewRobotCommand() { NewRobotEnergy = 200 }.NewRobotEnergy,
                            ((CreateNewRobotCommand)algorithm.DoStep(robots, robots.IndexOf(robot), map)).NewRobotEnergy);

            robot.Energy = 622;

            Assert.AreNotEqual(new CreateNewRobotCommand() { NewRobotEnergy = 500 }.NewRobotEnergy,
                            ((CreateNewRobotCommand)algorithm.DoStep(robots, robots.IndexOf(robot), map)).NewRobotEnergy);
            Assert.AreEqual(new CreateNewRobotCommand() { NewRobotEnergy = 200 }.NewRobotEnergy,
                            ((CreateNewRobotCommand)algorithm.DoStep(robots, robots.IndexOf(robot), map)).NewRobotEnergy);


            algorithm.RoundCount = 21;

            Assert.AreEqual(new CreateNewRobotCommand() { NewRobotEnergy = 500 }.NewRobotEnergy,
                            ((CreateNewRobotCommand)algorithm.DoStep(robots, robots.IndexOf(robot), map)).NewRobotEnergy);
            Assert.AreNotEqual(new CreateNewRobotCommand() { NewRobotEnergy = 200 }.NewRobotEnergy,
                            ((CreateNewRobotCommand)algorithm.DoStep(robots, robots.IndexOf(robot), map)).NewRobotEnergy);

        }

        [TestMethod]
        public void TestDoStepEnergyCollections()
        {
            EnergyStation station = new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(10, 10),
                RecoveryRate = 50
            };

            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Energy = 34,
                Position = station.Position,
                OwnerName = "Manzhula Vladislav"
            };

            Map map = new Map();
            IList<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();

            EnergyStation fatherestStation = new EnergyStation()
            {
                Energy = 1000,
                Position = new Position(10, 15),
                RecoveryRate = 50
            };

            map.Stations.Add(station);
            map.Stations.Add(fatherestStation);

            robots.Add(robot);

            Assert.AreEqual(new CollectEnergyCommand().GetType(),
                            (new ManzhulaAlgorithm().DoStep(robots, robots.IndexOf(robot), map)).GetType());

            station.Energy = 34;

            Assert.AreNotEqual(new CollectEnergyCommand().GetType(),
                            (new ManzhulaAlgorithm().DoStep(robots, robots.IndexOf(robot), map)).GetType());
        }
    }
}
