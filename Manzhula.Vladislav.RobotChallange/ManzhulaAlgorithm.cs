﻿using Manzhula.Vladislav.RobotChallange.Utils;
using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manzhula.Vladislav.RobotChallange
{
    public class ManzhulaAlgorithm : IRobotAlgorithm
    {
        public ManzhulaAlgorithm()
        {
            RoundCount = 0;
            RobotCount = 10;
            Logger.OnLogRound += Logger_OnLogRound;
        }

        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            RoundCount++;
        }

        public int RoundCount { get; set; }

        public int RobotCount { get; set; }

        public string Author => "Manzhula Vladislav";

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot robot = robots[robotToMoveIndex];

            EnergyStation station = FindUtils.getNearestStation(robot, map.Stations);

            if (FindUtils.getCellsInRadius(station.Position, 1).Contains(robot.Position))
            {
                if (robot.Energy > 320 && RoundCount < 45 && RobotCount < 100)
                {
                    if (RoundCount < 20)
                    {
                        RobotCount++;

                        return new CreateNewRobotCommand() { NewRobotEnergy = 200 };
                    }

                    if (robot.Energy > 620)
                    {
                        RobotCount++;

                        return new CreateNewRobotCommand() { NewRobotEnergy = 500 };
                    }
                }

                // if we spawned in not empty cell
                if (station.Energy > FindUtils.getRobotCountInRadius(station.Position, robots, 1) * 40)
                {
                    return new CollectEnergyCommand();
                }
                else
                {
                    List<EnergyStation> stationList = (List<EnergyStation>)map.Stations;
                    stationList.Remove(station);
                    station = FindUtils.getNearestFreeStation(robot, robots, stationList);
                }
            }

            Position newPos = station.Position;

            if (!FindUtils.isCellFree(newPos, robot, robots))
            {
                // will be going to last if all cells are not empty
                newPos = FindUtils.getFreeCellInRadius(newPos, 1, robot, robots);
            }

            newPos = DistanceUtils.getToStationFastestWay(newPos, robot);

            return new MoveCommand() { NewPosition = newPos };
        }
    }
}
