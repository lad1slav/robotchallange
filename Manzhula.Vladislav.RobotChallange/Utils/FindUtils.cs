﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manzhula.Vladislav.RobotChallange.Utils
{
    public class FindUtils
    {
        public static EnergyStation getNearestStation(Robot.Common.Robot movingRobot, IList<EnergyStation> stations)
        {
            EnergyStation nearestStation = stations.First();

            foreach (EnergyStation station in stations)
            {
                int distanceToStation = DistanceUtils.getDistance(movingRobot.Position, station.Position);
                int distanceToNearestStation = DistanceUtils.getDistance(movingRobot.Position, nearestStation.Position);

                if (distanceToStation < distanceToNearestStation)
                {
                    nearestStation = station;
                }
            }

            return nearestStation;
        }

        public static EnergyStation getNearestFreeStation(Robot.Common.Robot movingRobot,
                                                          IList<Robot.Common.Robot> robots,
                                                          IList<EnergyStation> stations)
        {
            List<EnergyStation> stationList = (List<EnergyStation>) stations;

            EnergyStation nearestFreeStation = getNearestStation(movingRobot, stationList);
            while (!isCellFreeInRadius(nearestFreeStation.Position, movingRobot, robots, 1))
                //&& station.Energy < 100 maybe
            {
                if (stationList.Count == 0)
                {
                    return null;
                }

                stationList.Remove(nearestFreeStation);
                nearestFreeStation = getNearestStation(movingRobot, stationList);
            }

            return nearestFreeStation;
        }

        public static bool isCellFree(Position cell,
                                      Robot.Common.Robot movingRobot,
                                      IList<Robot.Common.Robot> robots)
        {
            foreach (Robot.Common.Robot robot in robots)
            {
                if (robot != movingRobot && robot.Position == cell) return false;
            }

            return true;
        }

        public static bool isCellFreeInRadius(Position cell,
                                      Robot.Common.Robot movingRobot,
                                      IList<Robot.Common.Robot> robots, int radius)
        {
            foreach (Robot.Common.Robot robot in robots)
            {
                if (robot != movingRobot && getCellsInRadius(cell, radius).Contains(robot.Position)) return false;
            }

            return true;
        }

        public static int getRobotCountInRadius(Position cell,
                                      IList<Robot.Common.Robot> robots, int radius)
        {
            int count = 0;

            foreach (Robot.Common.Robot robot in robots)
            {
                if (getCellsInRadius(cell, radius).Contains(robot.Position))
                {
                    count++;
                }
            }

            return count;
        }

        public static List<Position> getCellsInRadius(Position cell, int radius)
        {
            List<Position> cells = new List<Position>();

            for (int x = cell.X - radius; x <= cell.X + radius; x++)
            {
                for (int y = cell.Y - radius; y <= cell.Y + radius; y++)
                {
                    cells.Add(new Position(x, y));
                }
            }

            return cells;
        }

        public static Position getFreeCellInRadius(Position cell, int radius,
                                                   Robot.Common.Robot robot, IList<Robot.Common.Robot> robots)
        {
            Position newPos = cell;

            foreach (Position currCell in getCellsInRadius(newPos, radius))
            {
                newPos = currCell;

                if (isCellFree(newPos, robot, robots))
                {
                    break;
                }
            }

            return newPos;
        }
    }
}
